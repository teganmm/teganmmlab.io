# Spindogs technical test

I was required to build a webpage from a static prototype, into a working page. The time limit was 4 hours, which is totally hard to stick to when you really get stuck into building something you are enjoying.

This project ignited the fire that this was what I wanted to do for a living.
